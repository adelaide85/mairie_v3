@extends('layouts.app')
<title>Création de compte réussi </title>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-body">
                  <b>  Votre compte a bien été crée !</b>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

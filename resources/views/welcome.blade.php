<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>Segré en Anjou Bleu</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color:#0277bd ;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }
            .text{
              font-size:45px;
              position :fixed;
              bottom:0px;
              margin:0 auto;
            }

            .links > a {
                color: #0277bd;
                padding: 0 25px;
                font-size: 11px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .img {
              height:800px;
              width: 1000px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}"><span class="glyphicon glyphicon-home"></span> Accueil</a>


                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        <a href="{{ url('/dashboard') }}"><i class="material-icons">view_module</i> Dashboard</a>
                        <a href="{{ url('/banniere') }}"> Banniere</a>
                        <a href="{{ url('/user') }}">Utilisateurs </a>
                        <a href="{{ url('/categorieDocuments') }}">Categorie Documents </a>
                        <a href="{{ url('/historique') }}">Historique </a>
                        <a href="{{ url('/documentsUploades') }}">Documents uploadés</a>
                          <a href="{{ url('/document') }}">Documents </a>
                        <a href="{{ url('/logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                                    <i class="material-icons">power_settings_new</i>  Deconnexion

                        </a>
                    @else
                        <a href="{{ url('/login') }}"><i class="material-icons">input</i> Connexion</a>

                    @endif
                </div>
            @endif
            <img class="img-responsive img-center" src="img/logo.png" alt="">
              <div class="content">



              <!--  <div class="title m-b-md">
                          Mairie : <br>
                    Segré En Anjou Bleu
                </div>-->

                <!--<div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>-->
            </div>
        </div>
    </body>
</html>


<title>Banniere</title>

@extends('template')

@section('contenu')

  <div class="col-sm-offset-3 col-sm-6">
    @if(session()->has('ok'))
        <div class="alert alert-success alert-dismissible">{!! session('ok') !!}</div>
    @endif
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title text-center">Gestion de la bannière</h3>
        </div>
        <table class="table">
            <thead>
      <tr>
        <th>#</th>
        <th>Texte</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($bannieres as $banniere)
      <tr>
        <td>{!! $banniere->id !!}</td>
        <td>{!! $banniere->texte !!}</td>
        <td> <a class="btn-floating btn-large red"> </a>{!! link_to_route('banniere.edit', 'modifier', [$banniere->id], ['class' => 'btn btn-success btn-block']) !!}</td>
      </tr>
    @endforeach
    </tbody>
    </table>
  </div>
</div>

@endsection

<title>Modification de la banniere</title>

@extends('template')

@section('contenu')
    <div class="col-sm-offset-4 col-sm-4">
        <br>
        <div class="panel panel-primary">
            <div class="panel-heading">Modification de la bannière</div>
            <div class="panel-body">
                <div class="col-sm-12">
                    {!! Form::model($banniere, ['route' => ['banniere.update', $banniere->id], 'method' => 'put', 'class' => 'form-horizontal panel']) !!}
                        <div class="form-group {!! $errors->has('texte') ? 'has-error' : '' !!}">
                            {!! Form::text('texte', null, ['class' => 'form-control', 'placeholder' => 'texte']) !!}
                            {!! $errors->first('texte', '<small class="help-block">:message</small>') !!}
                        </div>
                        {!! Form::submit('Envoyer', ['class' => 'btn btn-primary pull-right']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <a href="javascript:history.back()" class="btn btn-primary">
            <span class="glyphicon glyphicon-circle-arrow-left"></span> Retour
        </a>
    </div>
@endsection

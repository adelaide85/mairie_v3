
@extends('template')
<title>Modification d'un document</title>
@section('contenu')
    <div class="col-sm-offset-4 col-sm-4">
        <br>
        <div class="panel panel-primary">
            <div class="panel-heading">Modification d'un document</div>
            <div class="panel-body">
                <div class="col-sm-12">
                    {!! Form::model($document. ['route' => ['document..update', $document.->id], 'method' => 'put', 'class' => 'form-horizontal panel']) !!}
                        <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nom']) !!}
                            {!! $errors->first('name', '<small class="help-block">:message</small>') !!}
                        </div>
                        <div class="form-group {!! $errors->has('description') ? 'has-error' : '' !!}">
                            {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
                            {!! $errors->first('description', '<small class="help-block">:message</small>') !!}
                        </div>
                        <div class="form-group {!! $errors->has('idCateg') ? 'has-error' : '' !!}">
                            {!! Form::number('idCateg', null, ['class' => 'form-control', 'placeholder' => 'Categorie doc']) !!}
                            {!! $errors->first('idCateg', '<small class="help-block">:message</small>') !!}
                        </div>
                        {!! Form::submit('Modifier', ['class' => 'btn btn-primary pull-right']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <a href="javascript:history.back()" class="btn btn-primary">
            <span class="glyphicon glyphicon-circle-arrow-left"></span> Retour
        </a>
    </div>
@endsection

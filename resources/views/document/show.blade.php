
@extends('template')
<title>Infos document</title>
@section('contenu')
    <div class="col-sm-offset-4 col-sm-4">
        <br>
        <div class="panel panel-primary">
            <div class="panel-heading">Fiche du document</div>
            <div class="panel-body">
                <p>Nom : {{ $user->name }}</p>
                <p>Categorie doc : {{ $user->idCateg}}</p>
                <p>Etat : {{ $user->active }}</p>
                <p>Description: {{ $user->description}}</p>
                <p>Utilisateur  : {{ $user->utilisateur}}</p>
                <p>Date publication: {{ $user->datePubli }}</p>

            </div>
        </div>
        <a href="javascript:history.back()" class="btn btn-primary">
            <span class="glyphicon glyphicon-circle-arrow-left"></span> Retour
        </a>
    </div>
@endsection

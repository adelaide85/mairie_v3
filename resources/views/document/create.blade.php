@extends('template')
<title>Création d'un document</title>
@section('contenu')
    <div class="col-sm-offset-4 col-sm-4">
        <br>
        <div class="panel panel-primary">
            <div class="panel-heading">Création d'un document</div>
            <div class="panel-body">
                <div class="col-sm-12">
                    {!! Form::open(['route' => 'document.store', 'class' => 'form-horizontal panel']) !!}
                    <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nom']) !!}
                        {!! $errors->first('name', '<small class="help-block">:message</small>') !!}
                    </div>
                    <div class="form-group {!! $errors->has('active') ? 'has-error' : '' !!}">
                        {!! Form::number('active', null, ['class' => 'form-control', 'placeholder' => 'Etat']) !!}
                        {!! $errors->first('active', '<small class="help-block">:message</small>') !!}
                    </div>
                    <div class="form-group {!! $errors->has('idCateg') ? 'has-error' : '' !!}">
                        {!! Form::number('idCateg', null, ['class' => 'form-control', 'placeholder' => 'Categorie ']) !!}
                        {!! $errors->first('idCateg', '<small class="help-block">:message</small>') !!}
                    </div>
                    <div class="form-group {!! $errors->has('description') ? 'has-error' : '' !!}">
                        {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
                        {!! $errors->first('description', '<small class="help-block">:message</small>') !!}
                    </div>
                    <div class="form-group {!! $errors->has('description') ? 'has-error' : '' !!}">
                        {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
                        {!! $errors->first('description', '<small class="help-block">:message</small>') !!}
                    </div>
                    <div class="form-group {!! $errors->has('utilisateur') ? 'has-error' : '' !!}">
                        {!! Form::text('utilisateur', null, ['class' => 'form-control', 'placeholder' => 'Utilisateur']) !!}
                        {!! $errors->first('utilisateur', '<small class="help-block">:message</small>') !!}
                    </div>
                    <div class="form-group {!! $errors->has('datePubli') ? 'has-error' : '' !!}">
                        {!! Form::date('datePubli', null, ['class' => 'form-control', 'placeholder' => 'Date de publication']) !!}
                        {!! $errors->first('datePubli', '<small class="help-block">:message</small>') !!}
                    </div>
                    {!! Form::submit('Envoyer', ['class' => 'btn btn-primary pull-right']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <a href="javascript:history.back()" class="btn btn-primary">
            <span class="glyphicon glyphicon-circle-arrow-left"></span> Retour
        </a>
    </div>
@endsection

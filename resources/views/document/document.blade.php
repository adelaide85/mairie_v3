
@extends('template')
<title>Listes des documents </title>
@section('contenu')
    <br>
    <div class="col-sm-offset-2 col-sm-8">
        @if(session()->has('ok'))
            <div class="alert alert-success alert-dismissible">{!! session('ok') !!}</div>
        @endif
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title text-center"><b>Liste des documents</h3></b>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nom</th>
                        <th>Categorie</th>
                        <th>Etat</th>
                        <th>Description</th>
                        <th>Utilisateur</th>
                        <th>Date de publication</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($documents as $document)
                        <tr>
                            <td>{!! $document->id !!}</td>
                            <td class="text-primary"><strong>{!! $document->name !!}</strong></td>
                            <td>{!! $document->idCateg !!}</td>
                            <td>{!! $document->active !!}</td>
                            <td class="text-primary"><strong>{!! $document->description!!}</strong></td>
                            <td class="text-primary"><strong>{!! $document->utilisateur!!}</strong></td>
                            <td>{!! $document->datePubli !!}</td>


                            <td>{!! link_to_route('document.show', 'Voir', [$document->id], ['class' => 'btn btn-success btn-block']) !!}</td>
                            <td>{!! link_to_route('document.edit', 'Modifier', [$document->id], ['class' => 'btn btn-warning btn-block']) !!}</td>
                            <td>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['document.destroy', $document->id]]) !!}
                                    {!! Form::submit('Supprimer', ['class' => 'btn btn-danger btn-block', 'onclick' => 'return confirm(\'Vraiment supprimer ce document ?\')']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {!! link_to_route('document.create', 'Ajouter un document', [], ['class' => 'btn btn-info pull-right']) !!}
        {!! $documents->links() !!}
    </div>
@endsection

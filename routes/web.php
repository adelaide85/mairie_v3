<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
});

Route::get('/historique', function () {
    return view('historique');
});
Route::get('/categorieDocuments', function () {
    return view('categorieDocuments');
});
Route::get('/documentsUploades', function () {
    return view('documentsUploades');
});

Route::resource('user', 'UserController');
Route::resource('banniere', 'BanniereController');
Route::resource('document', 'DocumentController');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home2', 'Home2Controller@index')->name('home2');

<?php

namespace App\Http\Middleware;

use Closure;

class IsGerant
{
    

    public function handle($request, Closure $next)
    {
      if (session('statut')==='admin'|| session('statut')==='gerant'){
        return $next($request);
      }
        return redirect('/');
    }

}

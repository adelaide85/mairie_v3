<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BanniereUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'texte' => 'bail|required|max:255'. $this->banniere->id,
        ];
    }
}

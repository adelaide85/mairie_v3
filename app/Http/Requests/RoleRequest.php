<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{


    public function rules()
    {
        return [
        'admin' => 'bail|required|alpha|max:50',
        'gerant' => 'bail|required|alpha|max:50'      
        ];
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DocumentCreateRequest;
use App\Http\Requests\DocumentUpdateRequest;
use App\Repositories\DocumentRepository;
use App\Document;


class DocumentController extends Controller
{

      protected $documentRepository;
      protected $nbrPerPage = 15;


      public function __construct(DocumentRepository $documentRepository)
      {
          $this->documentRepository = $documentRepository;
      }

      public function index()
      {
          $documents = $this->documentRepository->getPaginate($this->nbrPerPage);


          return view('document.document', compact('documents'));
      }

      public function create()
      {
          return view('document.create');
      }

      public function store(DocumentCreateRequest $request,Document $document)
      {
            $document->name = $request->name;
            $document->utilisateur = $request->utilisateur;
            $document->description = $request->description;
            $document->idCateg = $request->idCateg;
            $document->datePubli= $request->datePubli;
            $document->save();
          return redirect()->route('document.index')->withOk("Le document " . $document->name . " a été créé.");
      }

      public function show($id)
      {
        $document = $this->getDocumentById($id);
          return view('document.show',  compact('document'));
      }

      public function edit( $id)
      {
        $document = $this->getDocumentById($id);
        $index = 1;
          return view('document.edit',  compact('document','index'));
      }

      public function update(DocumentUpdateRequest $request, $id)
      {
        $document = Document::find($id);
        $document->name = $documentUpdateRequest->name;
        $document->save();

          return redirect()->route('document.index')->withOk("Le document " . $request->name . " a été modifié.");
      }

      public function destroy(Document $document)
      {
          $this->documentRepository->destroy($document);

          return back()->withOk("Le document " . $document->name . " a été supprimé.");;
      }

      protected function getDocumentById($id)
      {
      return Document::find($id);
      }
  }

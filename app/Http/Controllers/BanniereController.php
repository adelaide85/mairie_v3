<?php

namespace App\Http\Controllers;

use Illuminate\Http\Requests;
use App\Repositories\BanniereRepository;
use App\Http\Requests\BanniereUpdateRequest;
use App\Banniere;


class BanniereController extends Controller
{

  protected $banniereRepository;
  protected $id;
      protected $nbrPerPage = 2;

      public function __construct(BanniereRepository $banniereRepository)
      {
          $this->banniereRepository = $banniereRepository;
      }

      public function index()
      {
          $bannieres = $this->banniereRepository->getPaginate($this->nbrPerPage);

          return view('banniere.banniere', compact('bannieres'));
      }

      public function edit(Banniere $banniere)
      {
        return view('banniere.edit',  compact('banniere'));
      }

      public function update(BanniereUpdateRequest $banniereUpdateRequest, Banniere $banniere)
      {

          $banniere->texte = $banniereUpdateRequest->texte;
          $banniere->save();

          return redirect()->route('banniere.index')->withOk("La bannière a été modifié.");
      }

      public function destroy(Banniere $banniere)
      {
          $this->banniereRepository->destroy($banniere);

          return back()->withOk("La bannière a été supprime.");
      }

      public function getBanniereById($id){
        return Banniere::find($id);
      }


  }

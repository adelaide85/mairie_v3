<?php

namespace App\Repositories;

use App\Role;
class RoleRepository
{
    protected $role;

    public function __construct(Role $role)
    {

        $this->role = $role;
    }
    public function all()
  {
      return $this->role->all();
  }
  public function allSelect()
  {
      $select = $this->all()->pluck('title', 'id');

      return compact('select');
  }
  public function update($inputs)
{
    foreach ($inputs as $key => $value) {
        $role = $this->role->whereSlug($key)->firstOrFail();
        $role->title = $value;
        $role->save();
    }

}

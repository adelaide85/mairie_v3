<?php

namespace App\Repositories;

use App\Document;


class UserRepository
{
    protected $document;


    public function __construct(Document $document)
    {
        $this->document = $document;

    }

    public function getPaginate($n)
    {
        return $this->document->paginate($n);
    }


    public function store(Array $inputs)
    {
        $inputs['name'];
        return $this->document->create($inputs);
    }

    public function save($document)
    {
        //return $this->restaurant->create($inputs);
    }


    public function update(Document $document, Array $inputs)
    {
      //  $document->update($inputs);
    }

    public function destroy(Document $document)
    {
      //  $document->delete();
    }
}

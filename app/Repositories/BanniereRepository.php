<?php

namespace App\Repositories;

use App\Banniere;

class BanniereRepository
{
    protected $banniere;
    protected $id;


    public function __construct(Banniere $banniere)
    {
        $this->banniere = $banniere;
    }

    public function getPaginate($n)
    {
       return $this->banniere->paginate($n);
    }
    public function save($banniere){

    }

    public function update(Banniere $banniere, Array $inputs)
    {
        //return $this->getById($id)->fill($inputs)->save();
    }
    public function destroy(Banniere $banniere)
    {
      //  $banniere->delete();
    }

}

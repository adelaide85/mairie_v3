<?php

namespace App\Repositories;

use App\User;
use DB;
use App\Role;
class UserRepository
{
    protected $user;
    protected $role;

    public function __construct(User $user,Role $role)
    {
        $this->user = $user;
        $this->role = $role;
    }

    public function getPaginate($n)
    {
        return $this->user->paginate($n);
    }

    protected function save($user, $inputs)
    {
        if (isset($inputs['seen'])) {
            $user->seen = $inputs['seen'] == 'true';
        } else {
            $user->username = $inputs['username'];
            $user->email = $inputs['email'];

            if (isset($inputs['role'])) {
                $user->role_id = $inputs['role'];
            } else {
                $role_user = $this->role->whereSlug('user')->first();
                $user->role_id = $role_user->id;
            }
        }

        $user->save();
    }

    public function getUsersWithRole($n, $role)
  {
      $query = $this->model->with('role')->oldest('seen')->latest();

      if ($role != 'total') {
          $query->whereHas('role', function ($q) use ($role) {
              $q->whereSlug($role);
          });
      }

      return $query->paginate($n);
  }


  public function count($role = null)
    {
        if ($role) {
            return $this->model
                ->whereHas('role', function ($q) use ($role) {
                    $q->whereSlug($role);
                })->count();
        }

        return $this->model->count();
    }

    public function counts()
  {
      $counts = [
          'admin' => $this->count('admin'),
          'gerant' => $this->count('gerant')      
      ];

      $counts['total'] = array_sum($counts);

      return $counts;
  }

    public function store(Array $inputs)
    {
        $inputs['password'] = bcrypt($inputs['password']);
        return $this->user->create($inputs);
    }

    public function update(User $user, Array $inputs)
    {
        $user->update($inputs);
    }

    public function destroy(User $user)
    {
        $user->delete();
    }
}
